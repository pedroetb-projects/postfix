# postfix

Configuration and deployment of an internal postfix mail relay

## Configure domain

You must set the following DNS records at your public domain, replacing variables between `<>`:

### DKIM

Used to verify content of signed emails. Full value can be found at log output and inside `dkim-vol` volume (`<domain>.txt` file).

* Type: `TXT`
* Name: `mail._domainkey.<domain>`
* Value: `"v=DKIM1; k=rsa; " "p=<public-key>" "<public-key-continuation>"`

### SPF

Used to prevent being considered as spam, validating which origins are authorized to send emails.

* Type: `TXT`
* Name: `<domain>`
* Value: `v=spf1 a mx include:_spf.google.com ~all`

### DMARC

Used to ensure DKIM and SPF, inform own spam policy and receive reports.

* Type: `TXT`
* Name: `_dmarc.<domain>`
* Value: `v=DMARC1; p=quarantine; adkim=r; aspf=r; rua=mailto:postmaster@<domain>; ruf=mailto:postmaster@<domain>`

## Test mail server

First, create a file `email.txt` with a email example (including required headers and content):

```sh
From: mail <mail@<domain>>
To: postmaster <postmaster@<domain>>
Subject: email test
Date: Wed, 21 Oct 2020 21:44:16

Test content.
```

Then, run this command from an allowed IP address to send a test email:

```sh
curl smtp://<domain> \
--mail-from mail@<domain> \
--mail-rcpt postmaster@<domain> \
--upload-file email.txt
```

All received emails will be resent to the external address, defined by the `EXTERNAL_EMAIL_ADDRESS` environment variable.

You can also send an email to `postmaster@<domain>` (or any defined alias account) directly from other email service provider.
